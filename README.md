# 自动签到
*by xl7@qq.com*

针对某个服务提供自动签到的功能。通过接口把相关账号密码添加至内容中，指定指定时间，通过定时任务机制完成自动签到功能。

## 技术选型
- Spring Framework
- Docker
- Hutool

## 其他说明
本项目主要展示方便使用hutool进行http请求，以及通过maven的docker插件自动把镜像文件发布到私服中去。

### docker-maven-plugin 插件说明
**serverId** 要在maven的配置文件中新增相关配置
```$xml
<server>
    <id>docker-aliyun</id>
    <username>xl7@qq.com</username>
    <password>******</password>
    <configuration>
        <email>xl7@qq.com</email>
    </configuration>
</server>
```
**dockerHost** 默认本地docker
