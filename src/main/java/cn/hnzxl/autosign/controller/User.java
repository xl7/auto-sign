package cn.hnzxl.autosign.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author: xl7@qq.com
 * @date: 2021-02-20 14:07
 * @description:
 */
@Data
@AllArgsConstructor
public class User {
    private String user;
    @JsonIgnore
    private String password;
}
