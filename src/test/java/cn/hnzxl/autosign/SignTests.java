package cn.hnzxl.autosign;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Test;

import javax.sound.midi.Soundbank;

/**
 * @author: xl7@qq.com
 * @date: 2021-02-20 11:55
 * @description:
 */

//@SpringBootTest
public class SignTests {

    public void sign(String u,String p) {
        String host = "https://honeybee.mifenglai.com";
        String logRes = HttpUtil.post(host+"/honeybee/user/loginByPwd", StrUtil.format("{\"mobile\":\"{}\",\"password\":\"{}\"}", u, p));
        JSON loginJSON = JSONUtil.parse(logRes);
        /**
        if(loginJSON.getByPath("")==null){
            System.out.println("没有购买");
        }*/
        String token = loginJSON.getByPath("data.token", String.class);
        String signInfo = HttpUtil.createGet(host + "/honeybee/user/signInfo").header("bees-token", token).execute().body();
        JSON signInfoJSON = JSONUtil.parse(signInfo);
        if(signInfoJSON.getByPath("data.isSignIn",Integer.class)== 0){
            Long uoId = signInfoJSON.getByPath("data.userDeviceSignInDtoList[0].uoId",Long.class);
            String body = HttpUtil.createPost(host + "/honeybee/user/sign?uoId=" + uoId).header("bees-token", token).execute().body();
            System.out.println(body);
        }else{
            System.out.println("over");
        }
    }
}
